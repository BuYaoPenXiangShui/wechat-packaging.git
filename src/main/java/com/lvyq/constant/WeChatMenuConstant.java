package com.lvyq.constant;

/**
 * @author lvyq
 * @version 1.0
 * @description: 自定义菜单
 * @date 2022/7/12 9:30
 */
public class WeChatMenuConstant {
    /**
     *创建自定义菜单- POST
     * access_token是公众号的全局唯一接口调用凭据，公众号调用各接口时都需使用access_token
     * access_token的有效期目前为2个小时，需定时刷新
     */
    public static final String CGI_BIN_MENU_CREATE= "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";

}
