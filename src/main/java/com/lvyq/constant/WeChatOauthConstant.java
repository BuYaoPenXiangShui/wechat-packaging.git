package com.lvyq.constant;

/**
 * @author lvyq
 * @version 1.0
 * @description: 网页授权常量类
 * @date 2021/2/19 9:20
 */
public final  class WeChatOauthConstant {

    private WeChatOauthConstant() {
    }
    /**
     *授权回调，换取code
     */
    public static final String OAUTH2_AUTHORIZE = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect";

    /**
     *通过code换取网页授权access_token
     */
    public static final String OAUTH2_ACCESS_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";


    /**
     *刷新access_token（如果需要）
     */
    public static final String OAUTH2_REFRESH_TOKEN = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=APPID&grant_type=refresh_token&refresh_token=REFRESH_TOKEN";


    /**
     *拉取用户信息(需scope为 snsapi_userinfo)-GET
     */
    public static final String SNS_USER_INFO = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";


    /**
     *检验授权凭证（access_token）是否有效-GET
     */
    public static final String SNS_AUTH = " https://api.weixin.qq.com/sns/auth?access_token=ACCESS_TOKEN&openid=OPENID";
}
