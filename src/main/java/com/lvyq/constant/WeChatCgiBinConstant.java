package com.lvyq.constant;

/**
 * @author lvyq
 * @version 1.0
 * @description: 全局共用常量类
 * @date 2021/2/19 14:21
 */
public final class WeChatCgiBinConstant {

    private WeChatCgiBinConstant() {
    }

    /**
     *获取Access_token- GET
     * access_token是公众号的全局唯一接口调用凭据，公众号调用各接口时都需使用access_token
     * access_token的有效期目前为2个小时，需定时刷新
     */
    public static final String CGI_BIN_TOKEN ="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";

    /**
     *获取sapi_ticket- GET
     * jsapi_ticket是公众号用于调用微信JS接口的临时票据。正常情况下，jsapi_ticket的有效期为7200秒，通过access_token来获取。
     */
    public static final String CGI_BIN_GET_TICKET ="https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";

    /**
     * @description: 通过模板id推送消息
     * @author: lvyq
     * @date: 2021/2/22 17:09
     * @version 1.0
     */
    public static final String MESSAGE_TEMP_SEND="https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";

    
}
