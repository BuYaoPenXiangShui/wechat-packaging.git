package com.lvyq.wx.model;

import java.util.List;

/**
 * @author lvyq
 * @version 1.0
 * @description: 通过网页授权获取的用户信息
 * @date 2021/2/20 13:37
 */
public class SNSUserInfo {
    /**用户标识  */
    private String openId;
    /**用户昵称  */
    private String nickname;
    /**性别（1是男性，2是女性，0是未知）  */
    private int sex;
    /**国家  */
    private String country;
    /**省份  */
    private String province;
    /**城市  */
    private String city;
    /**用户头像链接  */
    private String headImgUrl;
    /**用户特权信息  */
    private List<String> privilegeList;
    /**用户unionid,需要绑定开放平台才会获取到  */
    private String unionid;

    /**
     * 返回 用户标识
     *
     * @return 用户标识
     */
    public String getOpenId() {
        return openId;
    }
    /**
     * 设置 用户标识
     *
     * @param openId
     *            用户标识
     */

    public void setOpenId(String openId) {
        this.openId = openId;
    }
    /**
     * 返回 用户昵称
     *
     * @return 用户昵称
     */
    public String getNickname() {
        return nickname;
    }
    /**
     * 设置 用户昵称
     *
     * @param nickname
     *            用户昵称
     */

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    /**
     * 返回 性别（1是男性，2是女性，0是未知）
     *
     * @return 性别（1是男性，2是女性，0是未知）
     */
    public int getSex() {
        return sex;
    }
    /**
     * 设置 性别（1是男性，2是女性，0是未知）
     *
     * @param sex
     *            性别（1是男性，2是女性，0是未知）
     */

    public void setSex(int sex) {
        this.sex = sex;
    }
    /**
     * 返回 国家
     *
     * @return 国家
     */
    public String getCountry() {
        return country;
    }
    /**
     * 设置 国家
     *
     * @param country
     *            国家
     */

    public void setCountry(String country) {
        this.country = country;
    }
    /**
     * 返回 省份
     *
     * @return 省份
     */
    public String getProvince() {
        return province;
    }
    /**
     * 设置 省份
     *
     * @param province
     *            省份
     */

    public void setProvince(String province) {
        this.province = province;
    }
    /**
     * 返回 城市
     *
     * @return 城市
     */
    public String getCity() {
        return city;
    }
    /**
     * 设置 城市
     *
     * @param city
     *            城市
     */

    public void setCity(String city) {
        this.city = city;
    }
    /**
     * 返回 用户头像链接
     *
     * @return 用户头像链接
     */
    public String getHeadImgUrl() {
        return headImgUrl;
    }
    /**
     * 设置 用户头像链接
     *
     * @param headImgUrl
     *            用户头像链接
     */

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }
    /**
     * 返回 用户特权信息
     *
     * @return 用户特权信息
     */
    public List<String> getPrivilegeList() {
        return privilegeList;
    }
    /**
     * 设置 用户特权信息
     *
     * @param privilegeList
     *            用户特权信息
     */

    public void setPrivilegeList(List<String> privilegeList) {
        this.privilegeList = privilegeList;
    }

    /**
     * 返回 用户Unionid
     *
     * @return 用户Unionid
     */
    public String getUnionid() {
        return unionid;
    }

    /**
     * 设置用户Unionid
     *
     *
     */
    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

}
