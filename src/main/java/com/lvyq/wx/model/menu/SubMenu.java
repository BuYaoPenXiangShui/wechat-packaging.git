package com.lvyq.wx.model.menu;

/**
 * @author lvyq
 * @version 1.0
 * @description: 二级菜单数组，个数应为1~5个
 * @date 2022/7/12 11:00
 */
public class SubMenu {

    /**
     * 菜单标题，不超过16个字节，子菜单不超过60个字节
     */
    /**
     * 菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
     */
    private String type;

    private String name;
    /**
     * 菜单KEY值，用于消息接口推送，不超过128字节  click等点击类型必须
     */
    private String key;

    /**
     *网页 链接，用户点击菜单可打开链接，不超过1024字节。
     * type为miniprogram时，不支持小程序的老版本客户端将打开本url。
     * view、miniprogram类型必须
     */

    private String url;

    /**
     * miniprogram类型必须	小程序的appid（仅认证公众号可配置）
     */
    private String appid;

    /**
     * miniprogram类型必须
     * 小程序的页面路径
     */
    private String pagepath;

    /**
     * article_id类型和article_view_limited类型必须
     * 发布后获得的合法 article_id
     */
    private String article_id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getPagepath() {
        return pagepath;
    }

    public void setPagepath(String pagepath) {
        this.pagepath = pagepath;
    }

    public String getArticle_id() {
        return article_id;
    }

    public void setArticle_id(String article_id) {
        this.article_id = article_id;
    }
}
