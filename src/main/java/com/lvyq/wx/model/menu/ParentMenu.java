package com.lvyq.wx.model.menu;

import java.util.List;

/**
 * @author lvyq
 * @version 1.0 按钮 一级菜单数组，个数应为1~3个
 * @description: TODO
 * @date 2022/7/12 10:49
 */
public class ParentMenu extends SubMenu {

    /**
     * 二级菜单数组，个数应为1~5个
     */
    private List<SubMenu> sub_button;

    public List<SubMenu> getSub_button() {
        return sub_button;
    }

    public void setSub_button(List<SubMenu> sub_button) {
        this.sub_button = sub_button;
    }
}
