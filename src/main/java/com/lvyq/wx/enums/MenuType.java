package com.lvyq.wx.enums;

/**
 * @description: 菜单按钮类型
 * @author: lvyq
 * @date: 2022/7/12 9:53
 * @version 1.0
 */

public enum MenuType {

    /**
     *click的key可以自定义，也可以设置为123、abc等。用于自定义点击事件-常用
     */
    CLICK("click","click"),

    /**
     *跳转网页-常用
     */
    VIEW("view",null),
    /**
     * 扫码推事件用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后显示扫描结果（如果是URL，将进入URL），
     * 且会将扫码的结果传给开发者，开发者可以下发消息。
     */
    SCANCODE_PUSH("scancode_push","rselfmenu_0_1"),
    /**
     * 扫码推事件且弹出“消息接收中”提示框用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后，
     * 将扫码的结果传给开发者，同时收起扫一扫工具，然后弹出“消息接收中”提示框，随后可能会收到开发者下发的消息。
     */
    SCANCODE_WAITMSG("scancode_waitmsg","rselfmenu_0_0"),

    /**
     * 弹出系统拍照发图用户点击按钮后，微信客户端将调起系统相机，完成拍照操作后，会将拍摄的相片发送给开发者，
     * 并推送事件给开发者，同时收起系统相机，随后可能会收到开发者下发的消息。
     */
    PIC_SYSPHOTO("pic_sysphoto","rselfmenu_1_0"),

    /**
     * 弹出拍照或者相册发图用户点击按钮后，微信客户端将弹出选择器供用户选择“拍照”或者“从手机相册选择”。
     * 用户选择后即走其他两种流程。
     */
    PIC_PHOTO_OR_ALBUM("pic_photo_or_album","rselfmenu_1_1"),

    /**
     * 弹出微信相册发图器用户点击按钮后，微信客户端将调起微信相册，完成选择操作后，将选择的相片发送给开发者的服务器，
     * 并推送事件给开发者，同时收起相册，随后可能会收到开发者下发的消息。
     */
    PIC_WEIXIN("pic_weixin","rselfmenu_1_2"),

    /**
     * 弹出地理位置选择器用户点击按钮后，微信客户端将调起地理位置选择工具，完成选择操作后，
     * 将选择的地理位置发送给开发者的服务器，同时收起位置选择工具，随后可能会收到开发者下发的消息
     */
    LOCATION_SELECT("location_select","rselfmenu_2_0"),

    /**
     * 用户点击 article_id 类型按钮后，微信客户端将会以卡片形式，下发开发者在按钮中填写的图文消息
     */
    ARTICLE_ID("article_id",null),


    /**
     * 类似 view_limited，但不使用 media_id 而使用 article_id
     */
    ARTICLE_VIEW_LIMITED("article_view_limited",null);
    ;
    private String type;
    private String key;

    MenuType(String type, String key) {
        this.type = type;
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
