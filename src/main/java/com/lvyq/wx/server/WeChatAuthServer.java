package com.lvyq.wx.server;

import com.lvyq.constant.WeChatOauthConstant;

import java.net.URLEncoder;

/**
 * @author lvyq
 * @version 1.0
 * @description: 微信网页授权
 * @date 2021/2/20 9:19
 */
public class WeChatAuthServer {

    /**
     * @description: 获取授权回调地址
     * @author: lvyq
     * @date: 2021/2/20 9:35
     * @version 1.0
     */
    public static String getAuthorizeUrl(String APPID, String REDIRECT_URI, String SCOPE){
        String authorizeUrl = WeChatOauthConstant.OAUTH2_AUTHORIZE;
        authorizeUrl=authorizeUrl.replace("APPID",APPID).replace("REDIRECT_URI", URLEncoder.encode(REDIRECT_URI)).replace("SCOPE",SCOPE);
        return authorizeUrl;
    }
    /**
     * @description: 获取授权回调地址-静默授权（不弹出授权页面，直接跳转，只能获取用户openid）
     * @author: lvyq
     * @date: 2021/2/20 9:35
     * @version 1.0
     */
    public static String getAuthorizeUrlByBase(String APPID,String REDIRECT_URI){
        String authorizeUrl = WeChatOauthConstant.OAUTH2_AUTHORIZE;
        authorizeUrl=authorizeUrl.replace("APPID",APPID).replace("REDIRECT_URI", URLEncoder.encode(REDIRECT_URI)).replace("SCOPE","snsapi_base");
        return authorizeUrl;
    }

    /**
     * @description: 获取授权回调地址-非静默授权（弹出授权页面，可通过openid拿到昵称、性别、所在地。并且， 即使在未关注的情况下，只要用户授权，也能获取其信息 ）
     * @author: lvyq
     * @date: 2021/2/20 9:35
     * @version 1.0
     */
    public static String getAuthorizeUrlByUserInfo(String APPID,String REDIRECT_URI){
        String authorizeUrl = WeChatOauthConstant.OAUTH2_AUTHORIZE;
        authorizeUrl=authorizeUrl.replace("APPID",APPID).replace("REDIRECT_URI", URLEncoder.encode(REDIRECT_URI)).replace("SCOPE","snsapi_userinfo");
        return authorizeUrl;
    }



}
