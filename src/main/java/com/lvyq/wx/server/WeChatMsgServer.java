package com.lvyq.wx.server;

import com.alibaba.fastjson.JSONObject;
import com.lvyq.wx.model.Message;
import com.lvyq.wx.util.MsgUtil;

/**
 * @author lvyq
 * @version 1.0
 * @description: 消息类服务
 * @date 2021/2/22 17:11
 */
public class WeChatMsgServer {

    /**
     * @description: 根据模板id推送消息
     * @author: lvyq
     * @date: 2021/2/22 17:13
     * @version 1.0
     */
    public static JSONObject templateSend(Message vo, String accessToken){
       return MsgUtil.sendMessagePush(vo,accessToken);
    }
}
