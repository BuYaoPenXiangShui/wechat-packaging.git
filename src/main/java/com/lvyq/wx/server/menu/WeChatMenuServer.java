package com.lvyq.wx.server.menu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lvyq.constant.WeChatMenuConstant;
import com.lvyq.util.HttpRequestUtil;
import com.lvyq.wx.model.menu.ParentMenu;
import com.lvyq.wx.server.WeChatCgiBinServer;

import java.util.List;

/**
 * @author lvyq
 * @version 1.0
 * @description:自定义菜单
 * @date 2022/7/12 11:03
 */
public class WeChatMenuServer {

    /**
     * @description: 创建菜单
     * @author: lvyq
     * @date: 2022/7/12 11:05
     * @version 1.0
     */
    public static JSONObject menuCreatWithAccessToken(List<ParentMenu> button, String accessToken){
        JSONObject mav = new JSONObject();
        mav.put("button", JSON.toJSON(button));
       String CGI_BIN_MENU_CREATE = WeChatMenuConstant.CGI_BIN_MENU_CREATE.replace("ACCESS_TOKEN",accessToken);
       return HttpRequestUtil.httpsRequest(CGI_BIN_MENU_CREATE,"POST",mav.toJSONString());
    }

    /**
     * @description: 创建菜单-内部已经调用accessToken
     * @author: lvyq
     * @date: 2022/7/12 11:05
     * @version 1.0
     */
    public static JSONObject menuCreat(List<ParentMenu> button,String appId,String appSecret){
        JSONObject mav = new JSONObject();
        mav.put("button", JSON.toJSON(button));
        String accessToken = WeChatCgiBinServer.getToken(appId,appSecret);
        String CGI_BIN_MENU_CREATE = WeChatMenuConstant.CGI_BIN_MENU_CREATE.replace("ACCESS_TOKEN",accessToken);
        return HttpRequestUtil.httpsRequest(CGI_BIN_MENU_CREATE,"POST",mav.toJSONString());
    }

}
