package com.lvyq.wx.server;

import com.lvyq.wx.util.CgiBinUtil;

/**
 * @author lvyq
 * @version 1.0
 * @description: 基础接口调用
 * @date 2021/2/20 15:09
 */
public class WeChatCgiBinServer {
    
    /**
     * @description: 获取token。已缓存
     * @author: lvyq
     * @date: 2021/2/20 15:20
     * @version 1.0
     */
    public static String  getToken(String appId,String appSecret){
       String token=CgiBinUtil.getToken(appId,appSecret);
       return token;
    }

    /**
     * @description: 获取ticket。已缓存
     * @author: lvyq
     * @date: 2021/2/20 15:20
     * @version 1.0
     */
    public static String  getTicket(String accessToken){
       String ticket=CgiBinUtil.getTicket(accessToken);
       return ticket;
    }

}
