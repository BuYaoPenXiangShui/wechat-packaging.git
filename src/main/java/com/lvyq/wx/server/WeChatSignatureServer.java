package com.lvyq.wx.server;

import com.alibaba.fastjson.JSONObject;
import com.lvyq.util.CommonUtils;
import com.lvyq.wx.util.CgiBinUtil;

import java.security.MessageDigest;

/**
 * @author lvyq
 * @version 1.0
 * @description: 签名服务
 * @date 2021/2/22 10:12
 */
public class WeChatSignatureServer {

    /**
     * @description:签名生成规则
     * 1.参与签名的字段包括noncestr（随机字符串）, 有效的jsapi_ticket, timestamp（时间戳）, url（当前网页的URL，不包含#及其后面部分）
     * 2.对所有待签名参数按照字段名的ASCII 码从小到大排序（字典序）后，使用URL键值对的格式（即key1=value1&key2=value2…）拼接成字符串string1。这里需要注意的是所有参数名均为小写字符。
     * 3.对string1作sha1加密，字段名和字段值都采用原始值，不进行URL 转义。
     */


    /**
     * @description: 获取签名，通过appId和secert
     * @author: lvyq
     * @date: 2021/2/22 10:43
     * @version 1.0
     */
    public static JSONObject getSignature(String AppId,String SECRET,String url){
        String  access_token = CgiBinUtil.getToken(AppId, SECRET);
        String jsapiTicket = CgiBinUtil.getTicket(access_token);
        JSONObject mav = new JSONObject();
        //随机码
        String nonceStr = CommonUtils.getRandomStringByLength(12);
        //时间戳
        String timestamp = Long.toString(System.currentTimeMillis() / 1000);
        String signature = "";
        String sign = "jsapi_ticket=" + jsapiTicket + "&noncestr=" + nonceStr + "&timestamp=" + timestamp + "&url=" + url;
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(sign.getBytes("UTF-8"));
            signature = CommonUtils.bytesToHexString(crypt.digest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mav.put("state", true);
        mav.put("appId", AppId);
        mav.put("timestamp", timestamp);
        mav.put("nonceStr", nonceStr);
        mav.put("signature", signature);
        mav.put("jsapi_ticket",jsapiTicket);
        return mav;
    }


    /**
     * @description: 通过jsapiTicket签名
     * @author: lvyq
     * @date: 2021/2/22 10:42
     * @version 1.0
     */
    public static JSONObject getSignatureByTicket(String AppId,String jsapiTicket,String url){
        JSONObject mav = new JSONObject();
        //随机码
        String nonceStr = CommonUtils.getRandomStringByLength(12);
        //时间戳
        String timestamp = Long.toString(System.currentTimeMillis() / 1000);
        String signature = "";
        String sign = "jsapi_ticket=" + jsapiTicket + "&noncestr=" + nonceStr + "&timestamp=" + timestamp + "&url=" + url;
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(sign.getBytes("UTF-8"));
            signature = CommonUtils.bytesToHexString(crypt.digest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mav.put("state", true);
        mav.put("appId", AppId);
        mav.put("timestamp", timestamp);
        mav.put("nonceStr", nonceStr);
        mav.put("signature", signature);
        mav.put("jsapi_ticket",jsapiTicket);
        return mav;
    }


}
