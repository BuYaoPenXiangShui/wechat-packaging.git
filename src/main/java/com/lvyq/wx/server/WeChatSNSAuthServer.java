package com.lvyq.wx.server;


import com.alibaba.fastjson.JSONObject;
import com.lvyq.wx.model.SNSUserInfo;
import com.lvyq.wx.model.WeixinOauth2Token;
import com.lvyq.wx.util.SNSAuthUtil;

/**
 * @author lvyq
 * @version 1.0
 * @description: TODO 网页授权，获取相关信息
 * @date 2021/2/20 10:18
 */
public class WeChatSNSAuthServer {
    
    /**
     * @description: 获取网页授权凭证，包括access_token 和openid
     * @author: lvyq
     * @date: 2021/2/20 10:21
     * @version 1.0
     */
    public static WeixinOauth2Token getOauth2TokenByCode(String appId, String appSecret,String Code){
        String access_token=null;
        WeixinOauth2Token weixinOauth2Token = SNSAuthUtil.getOauth2AccessToken(appId,appSecret,Code);
        return weixinOauth2Token;
    }
    
    /**
     * @description: 获取refresh_token 用于刷新access_token（如果需要）
     * @author: lvyq
     * @date: 2021/2/20 11:45
     * @version 1.0
     */
    public static String getRefreshTokenByCode(String appId, String appSecret,String Code){
        String refresh_token=null;
        WeixinOauth2Token weixinOauth2Token = SNSAuthUtil.getOauth2AccessToken(appId,appSecret,Code);
        if (weixinOauth2Token !=null){
            refresh_token = weixinOauth2Token.getRefreshToken();
        }
        return refresh_token;
    }

    /**
     * @description: 获取openId
     * @author: lvyq
     * @date: 2021/2/20 10:21
     * @version 1.0
     */
    public static String getOpenIdByCode(String appId, String appSecret,String Code){
        String openId=null;
        WeixinOauth2Token weixinOauth2Token = SNSAuthUtil.getOauth2AccessToken(appId,appSecret,Code);
        if (weixinOauth2Token !=null){
            openId = weixinOauth2Token.getOpenId();
        }
        return openId;
    }

    /**
     * @description: 获取微信用户信息-GET 网页授权作用域为snsapi_userinfo
     * @author: lvyq
     * @date: 2021/2/20 13:41
     * @version 1.0
     */
    public static SNSUserInfo getSnsUserInfo(String openId,String accessToken){
        SNSUserInfo snsUserInfo = SNSAuthUtil.getSnsUserInfo(openId,accessToken);
        return snsUserInfo;
    }

    /**
     * @description: 刷新access_token
     * refreshToken 通过access_token获取到的refresh_token参数
     * 由于access_token拥有较短的有效期，当access_token超时后，可以使用refresh_token进行刷新，
     * refresh_token有效期为30天，当refresh_token失效之后，需要用户重新授权。
     * @author: lvyq
     * @date: 2021/2/20 13:12
     * @version 1.0
     */
    public static JSONObject refreshToken(String appId,String refreshToken){
        JSONObject mav = SNSAuthUtil.refreshToken(appId,refreshToken);
        return mav;
    }

    /**
     * @description: 检验授权凭证（access_token）是否有效
     * 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     * @author: lvyq
     * @date: 2021/2/20 13:20
     * @version 1.0
     */
    public static JSONObject checkSnsAuth(String openId,String accessToken){
        JSONObject mav = SNSAuthUtil.checkSnsAuth(openId,accessToken);
        return mav;
    }




}
