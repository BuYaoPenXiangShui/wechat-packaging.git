package com.lvyq.wx.server;

import com.lvyq.wx.util.TokenCheckUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * @program: wx-control-centre
 * @description: token验证(服务器配置)
 * @author: lvyq
 * @create: 2023-03-09 16:17
 **/
public class WeChatTokenCheckServer {

   
    /** 
    * @Description: token验证
    * @params:  * @param request
    * @param token  微信公众平台服务器配置的token
    * @return: {@link boolean}
    * @Author: lvyq
    * @Date: 2023/3/9 16:35
    */
    public static boolean checkToken(HttpServletResponse response,HttpServletRequest request, String token) throws NoSuchAlgorithmException, IOException {
       return TokenCheckUtils.checkToken(response,request,token);
    }
}
